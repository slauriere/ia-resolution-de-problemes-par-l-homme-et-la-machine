
%Intelligence artificielle -- Résolution de problèmes par l'Homme et la machine
%par Jean-Louis Laurière, Professeur à l'Université Pierre et Marie Curie
%Groupe de Recherche CNRS, C.F PICARD-PARIS.
%Ouvrage initialement diffusé par les Editions EYROLLES (EYROLLES, 61, boulevard Saint-Germain - 75005 Paris)



%\clearpage

\section*{Avant-propos}

La raison d'être de ce livre remonte sans doute à l'époque où j'étais lycéen; comme tous les écoliers autour de moi j'ingurgitais le contenu des programmes scientifiques en maths et en physique en me demandant sans cesse: \textbf{à quoi cela peut-il bien servir?} En physique, le rapport avec la réalité se voulait évident, mais nous n'arrivions pas à y croire; trop de choses restaient mystérieuses. De plus, chaque année, le nouveau professeur nous expliquait que le modèle de l'année précédente était faux... La physique était perçue comme un jeu bizarre du monde des adultes. En mathématiques, la situation était différente: la beauté de l'abstraction, tout autant que le plaisir de trouver une démonstration élégante étaient motivants. Un sentiment profond de duperie venait cependant gâcher tout cela: on nous assénait sans cesse des définitions et des preuves comme des réalités révélées; \textbf{le pourquoi des choses n'était jamais donné}. La plupart des preuves paraissait sortir par magie du bâton de craie du professeur. Comment pouvait-on enchaîner toutes ces lignes et penser dès le début à l'aboutissement merveilleux de la dernière d'entre elles ? Et par dessus tout, \textbf{\og à quoi cela pouvait-il servir\fg}?

En réalité cela devint clair, plus tard, après quelques années de vie \og active\fg -- \textbf{tout cela ne sert à rien}, ou, du moins, ne sert pas directement: tous ces sujets d'études sont arbitrairement choisis et placés dans les programmes scolaires. En vérité, ce ne sont que des prétextes pour faire passer quelque chose de plus noble: apprendre à comprendre, apprendre à résoudre des problèmes, \textbf{apprendre à apprendre}. Mais, curieusement, ces sujets-là, justement, ne sont nullement avoués et guère enseignés. Une certaine forme de terrorisme intellectuel classe définitivement une bonne part des écoliers \og nuls en maths\fg, alors que leur seul tort est de ne pas comprendre... ce qui n'a jamais été dit. D'autres s'en tirent parce qu'ils ont entrevu plus tôt la règle du jeu implicite. D'autres encore apprennent tout par coeur...

Or, il existe actuellement un domaine de recherche, où l'intention première des chercheurs est précisément de comprendre comment un système de traitement de l'information -- homme ou machine -- peut assimiler, analyser, transposer, généraliser ce qui lui est enseigné et, par là, faire face à des situations concrètes et résoudre des problèmes.

Le nom de cette discipline est \textbf{l'intelligence artificielle;} c'est la fille aînée de l'informatique: elle a pour sujet d'étude toutes les activités intellectuelles de l'homme pour lesquelles aucune méthode n'est a priori connue. C'est ainsi que son objet a pu être défini par \og tout ce qui n'a pas encore été fait en informatique\fg. Si l'informatique est la science du traitement de l'information, l'intelligence artificielle -- I.A. dans la suite -- s'intéresse à tous les cas où ce traitement ne peut être ramené à une méthode simple, précise, algorithmique.

Ces cas sont innombrables, même dans des situations très banales comme la lecture d'un texte, tel celui-ci. Le même 
caractère, le | par exemple sera perçu, par le système visuel, suivant le contexte, tantôt comme un tantôt comme un i, 
tantôt comme un signe de valeur absolue... Le cas de l'écriture manuscrite est pire encore: les trois mots suivants 
utilisent ainsi le même graphème \includegraphics[height=0.3cm]{figures/0-0.png} : \begin{figure}[h!]
\centering \includegraphics[height=0.6cm]{figures/0-1.png}
\end{figure}

Comment notre système cognitif lève-t-il t'il l'ambiguïté ? Tel est un des problèmes clés soulevés en I.A. L'identification de visages humains (qui sont moins aisés à caractériser formellement que de simples lettres), la compréhension de textes (et non plus de lettres isolées), celle de paroles (sans trace écrite), la démonstration de théorèmes, la résolution de problèmes, le choix d'un coup au jeu d'échecs, la construction d'un emploi du temps, la réponse à un test de quotient intellectuel, la conception d'un plan en architecture, l'élaboration d'un diagnostic en médecine, l'analyse, d'un article de journal, sont autant de sujets d'étude dans cette discipline. Ils ont tous fait l'objet de réalisations récentes souvent impressionnantes.

