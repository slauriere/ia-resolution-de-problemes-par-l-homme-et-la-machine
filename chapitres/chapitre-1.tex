\chapter{Intelligence artificielle}\label{chapitre1}

\pagenumbering{arabic}

Les principaux sujets d'étude de l'Intelligence artificielle sont l'acquisition et la représentation de la \textbf{connaissance} sous toutes ses formes.

L'objectif des chercheurs en I.A. n'est pas seulement d'élaborer de nouvelles théories, mais également de concevoir et de mettre en oeuvre des programmes aussi généraux que possible.

Nous examinerons tout d'abord le rôle joué par l'informatique traditionnelle. Nous verrons ensuite pourquoi et comment les objectifs de l'I.A. s'écartent de cette démarche classique pour apporter un renouveau fondamental.

\section{Statut de l'informatique}\label{Statut de l'informatique}

L'informatique est utilisée de deux façons différentes.

\begin{enumerate}
\item En tant que support pour une modélisation: c'est alors un prolongement des mathématiques qui met l'accent sur le phénomène temps et les processus évoluant dans le temps. La solution d'un problème ayant été analysée mathématiquement, elle est traduite par un programme exécutable par un ordinateur.
\item En tant qu'outil concret de vérification d'hypothèses: un ordinateur est, par construction, impartial et fidèle; c'est une merveilleuse machine pour \textbf{tester des idées.}
\end{enumerate}

Ainsi, l'informatique, et plus encore l'I.A., entretiennent-elles des liens privilégiés avec la linguistique, la psychologie et la logique. Ces trois disciplines, en effet, s'intéressent aux phénomènes cognitifs, à la compréhension, au raisonnement.

Il est remarquable que ces liens jouent dans les deux sens: premièrement, aujourd'hui, les linguistes, les psychologues, les logiciens \textbf{programment} les modèles nouveaux qu'ils construisent (les biologistes, les médecins, les mathématiciens tendent à faire de même). Deuxièmement, les chercheurs en I.A. reprennent ces \textbf{modèles} et tentent d'en déduire des \textbf{logiciels} effectifs de résolution de problèmes.

Il existe des rapports particulièrement étroits entre I'I.A. et les sciences cognitives: la raison en est 
simple: à part l'ordinateur, le seul outil à raisonner auquel nous ayons accès est notre cerveau, et encore ce dernier est-il d'un abord difficile, pour ne pas dire impossible. Toute introspection gène l'expérience en cours. L'ordinateur fait donc figure d'auxiliaire extraordinaire.


Du même coup, l'I.A., après les révolutions fondamentales de Copernic et Darwin, entraîne une
remise en cause de la place de l'homme dans la nature. C'est bien, en effet, ici le monopole de
l'intelligence qui lui est contesté.

\section{L'intelligence artificielle}\label{L'intelligence artificielle}

L'I.A. est une science qui date d'une trentaine d'années. Son objet est de reconstituer à l'aide de moyens artificiels -- presque toujours des ordinateurs -- des raisonnements et des actions intelligentes. Les difficultés sont a priori de deux types:

\begin{enumerate}
\item Pour la plupart de nos activités nous ne savons pas nous-mêmes comment nous nous y prenons. Nous ne connaissons pas de méthode précise -- pas d'algorithme disent aujourd'hui les informaticiens -- pour comprendre un texte écrit ou reconnaître un visage, démontrer un théorème, établir un plan d'action, résoudre un problème, apprendre...
\item Les ordinateurs sont a priori très loin d'un tel niveau de compétence. Il faut les programmer depuis le tout début. Les langages de programmation ne permettent, en effet, que d'exprimer des notions très élémentaires.
\end{enumerate}

L'I.A. est, de ce double point de vue, une science expérimentale: expériences sur ordinateurs qui permettent de tester et d'affiner les modèles exprimés dans les programmes sur de nombreux exemples; observations sur l'homme (en général le chercheur lui-même) pour découvrir ces modèles et mieux comprendre le fonctionnement de l'intelligence humaine.

Dans le premier paragraphe nous préciserons la définition et les domaines de I'I.A. Nous relaterons ensuite l'histoire de cette science nouvelle en mettant en évidence les lignes de force qui ont guidé les chercheurs. En dernier lieu, nous dresserons le bilan des méthodes actuelles, des résultats acquis aujourd'hui et des travaux pour demain.


\section{Définition et domaines de l'intelligence artificielle}\label{Définition et domaines de l'intelligence artificielle}


\subsection{Définition}

\textbf{Tout problème pour lequel aucune solution algorithmique n'est connue, relève a priori de
l'Intelligence artificielle}.

Par \textit{algorithme}, il faut entendre ici toute suite d'opérations ordonnées, bien définies, exécutables sur un ordinateur actuel et qui permet d'arriver à la solution en un temps raisonnable (de l'ordre de la minute ou de l'heure). Ainsi, on ne connaît pas d'algorithme pour jouer aux échecs, bien que ce jeu 
ne comporte qu'un nombre fini de situations, mais les étudier toutes demanderait des millénaires. De même, il n'existe pas d'algorithme pour effectuer un diagnostic médical, résumer un texte ou le traduire dans une langue étrangère.

\clearpage

\subsection{Domaines}

Ces domaines -- très variés -- où nous agissons sans méthode absolument définie, possèdent en commun deux caractéristiques remarquables:

\liststyleRTFNumiii
\begin{enumerate}
\item ils concernent des informations \textbf{symboliques}: lettres, mots, signes, dessins. Ceci s'oppose aux traitements numériques habituellement confiés aux ordinateurs,
\item ils impliquent des \textbf{choix}. En effet, dire qu'il n'existe pas d'algorithme, c'est dire qu'à certains pas, il faut choisir sans certitude entre plusieurs possibilités. Ce non-déterminisme fondamental, cette liberté d'action est une \textbf{composante essentielle de l'intelligence.}
\end{enumerate}

Le premier problème auquel se heurte la recherche en I.A. est celui de la saisie de l'information. Les technologies et les logiciels actuels sont loin de pouvoir rivaliser avec les capteurs et effecteurs humains en vision, manipulation, goût, odorat, compréhension ou émission de parole.


\subsubsection{La perception et la reconnaissance des formes}

Un système de traitement de l'information reçoit des données de ses capteurs. De nos cinq sens,
l'oeil -- et le système visuel -- est sans doute le plus important. Les caméras et les lasers sont couramment connectés aujourd'hui à des programmes de \og reconnaissance d'images\fg et \og d'analyse de scènes\fg. Des microphones servent d'autre  part à capter des sons.

Ce domaine du codage et du traitement des signaux est connu sous le nom de \textbf{reconnaissance des formes}. C'est un point de passage obligatoire pour un système autonome de traitement de l'information. Mais, en réalité, les problèmes qui se posent en I.A. sont loin d'être résolus, même si l'information est déjà codée et mémorisée. Aussi, ce sont aux problèmes de compréhension et de raisonnement qui viennent logiquement ensuite, que s'attaquent spécifiquement les travaux d'I.A.


\subsubsection{Les mathématiques et la démonstration automatique de théorèmes}

L'I.A. attache, par définition, une importance particulière à \textbf{l'information symbolique}, c'est à dire non numérique. Ainsi, les premiers domaines où les chercheurs ont travaillé furent les mathématiques
et les jeux. Dans les deux cas, en effet, ces domaines, déjà bien formalisés, et qui, en outre, font figure de bastion de l'intelligence humaine, sont d'excellents sujets tests.

Les premiers programmes en démonstration automatique virent le jour en 1957, soit 10 ans après l'apparition des premiers ordinateurs. Ils travaillaient d'abord sur des
%P4
théories limitées, puis, de plus en plus riches. Le niveau de l'individu moyen fut rapidement dépassé. Mais celui du bon mathématicien n'est toujours pas atteint aujourd'hui! La théorie de la preuve et celle des méthodes effectives de démonstration furent étudiées de plus près et plusieurs développements furent apportés. De plus, les formalisations mathématiques de type logique se révélaient indispensables pour les applications fondamentales: robotique, résolution de problèmes, interrogation de bases de données. Ce sujet reste ainsi l'un des domaines phare en I.A.

\subsubsection{Les jeux}

Plus encore que les systèmes formels des mathématiques, les jeux concernent des univers limités, aux régles bien spécifiées et pourtant riches en possibilités déductives. C'est pourquoi ils furent et sont encore un sujet de prédilection en I.A. Ici encore, le niveau du joueur de club moyen fut aisément
dépassé, tandis que celui du champion du monde ne l'est pas encore. De façon inattendue, les difficultés rencontrées à propos des jeux furent, en effet, les mêmes qu'en mathématiques et que dans beaucoup d'autres domaines. Elles ont toujours trait à la \textbf{masse considérable de connaissances} que l'homme a su accumuler à travers les âges. Pour les jeux de hasard, tel le poker ou le backgammon, où le raisonnement s'efface devant le calcul des probabilités, les performances des programmes sont, en revanche, remarquables.


\subsubsection{La résolution de problèmes}

Le mot résolution est à prendre dans son sens large. Il s'agit de poser, d'analyser et de représenter des situations concrètes, tout autant que de les résoudre. Le bilan, très restreint, des problèmes aujourd'hui bien résolus une fois fait. Il reste le vaste champ de tous les autres. C'est la capacité d'invention et de généralisation qui est ici en cause, qu'il s'agisse de problèmes de la vie de tous les jours, de recherche opérationnelle ou de mathématiques.

Les robots doivent en particulier être capables de résoudre des problèmes dont la solution nous vient inconsciemment: monter sur quelque chose pour saisir un objet, allumer la lumière pour y voir clair.


\subsubsection{Compréhension du langage naturel (LN)}

Dans la partie \og compréhension du langage naturel\fg, les chercheurs s'intéressent
à l'analyse et à la génération de textes, à leur représentation interne, à la mise à jour des connaissances nécessaires à leur compréhension: connaissances syntaxiques, sémantiques, pragmatiques. Cette partie n'est pas  traitée dans ce livre : Cf: Pitrat J. (1985): \textit{Textes, ordinateurs et compréhension}, Eyrolles.

%%P5

Les retombées et les implications sociales des recherches en I.A. seront bientôt importantes.

Ainsi, plusieurs disciplines sont désormais directement concernées: la psychologie (le cerveau humain reste le point de passage obligatoire pour faire de l'I.A.), la logique, la linguistique, la biologie (modèles de transmission d'information par les gènes), l'informatique (systèmes d'exploitations évolués, interrogation de bases de données, programmation automatique), la médecine (aide au diagnostic) et surtout peut-être la didactique et la pédagogie dans toutes les disciplines (le niveau de détail des programmes d'I.A. met en relief certaines carences des enseignements traditionnels et met en évidence tout ce qui n'est pas assez enseigné).

\section{Historique}
\label{Historique}

Les ordinateurs, s'ils ne sont pas indispensables pour construire et tester les modèles d'I.A., sont un
moyen de recherche formidable et, concrètement, c'est avec eux que l'I.A. a pris son essor. Dès 1954, A. Newell fait le projet d'écrire un programme pour bien jouer aux échecs. C. Shannon, père de la théorie de l'information, avait déjà proposé une méthode pour cela. A. Turing, un des premiers informaticiens, l'avait affinée et simulée à la main. A la Rand Corporation, J. Shaw et H. Simon s'associent au projet de Newell. Ils s'assurent la collaboration d'une équipe de psychologues d'Amsterdam, dirigée par A. de Groot, qui avait étudié de grands joueurs humains. Un langage est tout spécialement créé par cette équipe pour manipuler facilement en machine les informations symboliques, les pointeurs, les listes
chaînées: c'est IPL1 (Information Processing Language 1, 1956) qui sera le père de LISP (J. Mac Carthy 1960). Le premier programme d'I.A. est finalement le \og LOGIC THEORIST\fg qui travaille en logique des propositions et donne \textbf{sa première démonstration le 9 août 1956.}

Le programme d'échecs NSS (Newell, Shaw, Simon ) voit le jour en 1957. Sa structure et celle du LOGIC THEORIST. Les notions de \og situation souhaitable\fg et d'\textit{\og heuristiques\fg} (du grec euriskein: trouver = règle qui permet de faire un choix en l'absence de résultat théorique sûr) conduisent, un peu plus tard, à la conception de GPS, \og GENERAL PROBLEM SOLVER\fg. Ce programme, par analyse de différences entre situations et par construction de buts, sait résoudre aussi bien le casse tête des \og Tours de Hanoï\fg, qu'intégrer formellement une expression.

Les informaticiens s'intéressent tous alors à l'I.A, certains écrivent déjà des essais célèbres, J. Mac Carthy, M. Minsky, H. Simon. D'autres programmes voient le jour. Gelernter (1960), à propos de géométrie, constate qu'un programme peut mieux faire que son programmeur! Pour démontrer que le triangle ABC, qui a les deux angles à la base B et C égaux, a deux côtés égaux, le programme, au lieu de la démonstration classique des livres, qui consiste à construire la hauteur en A, applique simplement un théorème d'égalité des triangles ABC et ACB ! Le résultat est évident... Le programme EPAM (Elementary Perceiving and Memorizing Program) est conçu par E. Feigenbaum comme une simulation psychologique.


%P6

Le traitement du langage est entrepris très tôt avec une application sur la recherche d'information dans les bases de données: BASEBALL (Green et al 1961) répond aux questions sur les matches. La résolution de problèmes d'algèbre, posés en anglais, est à la portée du système STUDENT (Bobrow 1964).

Les ambitions en traduction automatique sont grandes. Les projets dans ce domaine occupent de nombreuses équipes. Les chercheurs croient alors avant tout à l'analyse syntaxique et à l'information donnée par les dictionnaires (méthodes par \textbf{mots-clés)}.

Cela ne suffit pas, comme de sévères rapports le mettront en lumière (Dreyfus 1972, Lighthill 1973), mais les chercheurs mettront des années à se rendre compte que la traduction automatique n'est pas un problème en soi, et qu'elle passe nécessairement par la \textbf{compréhension.}

Un cadre logique nouveau, construit à partir d'une systématisation du raisonnement par l'absurde, naît en 1965 (J. Robinson). Il permet la formalisation de nombreux problèmes et leur interprétation en machine, il est utilisé aussi bien pour démontrer des théorèmes (Slagle, Green, Kowalski), que pour vérifier  des programmes (King, Waldinger), manipuler des objets (Nilsson, Fikes). Il est le point de départ d'un langage de programmation original: PROLOG, qui a la puissance de la logique du premier ordre, et a été conçu par A. Colmerauer en 1971. (Cf chapitre \ref{chapitre3}).


De façon constante, les recherches en I.A. sont ainsi jalonnées par des générations de langages et de systèmes, de plus en plus généraux, qui rapprochent la programmation des ordinateurs de notre raisonnement et de notre vocabulaire habituel. Outre LISP et PROLOG, citons les plus importants: PLANNER et QA4 permettent à l'aide des concepts de but et d'assertion de modéliser et de formaliser la déduction en résolution de problèmes; MACSYMA et REDUCE permettent la  manipulation formelle des expressions mathématiques; TMS permet de gérer des informations non sûres et de tester leur cohérence.

Les chercheurs en robotique utilisent, par ailleurs, ces résultats au fur et à mesure, pour piloter des systèmes fixes  ou mobiles dans l'univers réel. Ils font face en outre à tous les problème de capteurs. La vision par caméra, la détection des contours (Gussman, Waltz, Winston) et d'objets cachés, dans des univers complexes, sont des problèmes de mieux en mieux résolus.

Mais, jusqu'à cette époque (1968), les chercheurs ont essentiellement utilisé des micro-univers de travail: jeux, géométrie, calcul intégral, monde de blocs, phrases courtes sur petits vocabulaires. La méthode retenue est presque toujours la même; c'est \textbf{l'amélioration du combinatoire}: on restreint l'énumération exhaustive à l'aide du \og bon sens\fg, de fonctions d'évaluation numériques et d'heuristiques.

L'honnête homme, certes, ne se sert pas que de cela, mais des dizaines de réalisations ont été programmées dans de très nombreux univers dont les performances rivalisent effectivement avec les nôtres (les micro-ordinateurs de jeux -- Echecs, Dames,  Othello, Bridge, Go -- tombent dans cette catégorie). Il reste mieux à faire : atteindre le comportement des \textbf{experts}.

%P7

\section{Résultats}\label{Résultats}

Le début des années 70 marque une réelle transition dans les recherches en I.A. et ceci pour deux raisons.

En premier lieu, tous les chercheurs se sont, petit à petit, convaincus que tous les programmes précédents manquaient beaucoup d'une composante essentielle: la connaissance approfondie du domaine concerné. Ce qui fait la différence entre l'expert et l'homme de la rue, c'est \textbf{l'expérience}, autrement dit la connaissance accumulée au fil des années. Si l'on désire améliorer les performances d'un programme d'I.A., ce n'est plus sur l'ajustement des heuristiques ou des cœfficients numériques qu'il faut travailler, mais bien sur la \textbf{mise en oeuvre de raisonnements, et d'expériences symboliques}.

En second lieu, se pose un problème immédiat et concret: \textbf{comment donner cette connaissance à un programme} alors que le programmeur lui-même ne la possède pas?

La réponse est claire: le programme doit l'acquérir en donnée, de l'expert lui-même. Les chercheurs ont été ainsi conduits à doter les systèmes d'I.A. d'une possibilité que n'ont pas les langages de programmation standards: les programmes d'I.A. doivent pouvoir saisir une information comme \og Il fait beau, à Paris, ce 10 février\fg, la stocker et ne l'utiliser qu'à bon escient. Il y a ici séparation entre l'énoncé d'un fait et la manière de se servir de ce fait. Par opposition, un langage de programmation ne permet d'exprimer que des ordres \textbf{exécutables}.

Cette caractéristique est vitale puisque l'expert fournit des faits isolés, sans pouvoir dire par avance à quel moment il conviendra de les prendre en compte.

Les recherches en résolution de problèmes et en compréhension du langage naturel convergent alors vers cette question centrale de la \textbf{représentation des connaissances.}

Plusieurs programmes voient le jour vers 1970, qui sont fondés sur ces idées. Le premier est DENDRAL. Il déduit des formules chimiques développées de corps chimiques à partir de leur spectrographe de masse. Mis au point à Stanford avec l'aide de J. Lederberg, prix Nobel, il a été progressivement nourri de l'expérience de celui-ci. Il contient plusieurs milliers de faits élémentaires. C'est l'un des premiers \textit{systèmes experts}. Ces faits sont donnés sous forme de règles granulaires dans le langage même de l'expert. Ce système a des performances remarquables et est actuellement vendu avec le spectromètre par le fabricant.

Bien sûr, l'idéal est que le programme déduise lui-même les règles à partir de son expérience, en un mot, qu'il apprenne. C'est ce qui a été réalisé par l'équipe DENDRAL au SRI (Stanford Research Institute): METADENDRAL, par simulations répétées et à l'aide de quelques règles générales indiquant les familles de coupures possibles, infère progressivement les règles particulières pour chaque liaison chimique, puis pour chaque corps. Il a ainsi effectué, de toutes pièces, l'analyse de familles chimiques mal connues et permis ainsi la rédaction d'articles dans des revues internationales de chimie.

%P8

Terry Winograd est, quant à lui, l'auteur de SHRDLU (1971), un robot manipulateur de cubes, qui dialogue en anglais. Le système ne s'intéresse plus seulement à la syntaxe des phrases mais en comprend véritablement le sens grâce à des connaissances \textbf{sémantiques }et \textbf{pragmatiques} sur son univers de blocs. Il sait lever les ambiguïtés (par exemple les références pronominales), résoudre les métaphores, justifier son comportement et rendre compte de ses actions. En outre, il montre, sur l'univers réel, que tout ceci est bien géré par le programme.

\textbf{L'I.A. arrive aujourd'hui à un stade opérationnel.}

Les chercheurs à plein temps ne sont que quelques centaines au monde (dont une centaine en France), mais leurs résultats concernent chacun d'entre nous. Les médias en parlent beaucoup et annoncent souvent les \og robots\fg pour demain. Il faut, en fait, bien comprendre que cette recherche est lente et difficile: car, loin de chercher à inventer des recettes miracles (des algorithmes), les chercheurs en I.A. s'efforcent de reconstituer peu à peu et de mettre en machine l'expérience et la connaissance des spécialistes dans tous les domaines.

Or, en général, cette \textbf{information n'est pas disponible:} il faut longuement interroger l'expert et retrouver ce qu'il a mis inconsciemment des années à apprendre. Pour cette tâche, ont été développés des langages et des systèmes de représentation. Mais il faut \textit{rentrer} plus d'informations que ce que contient un dictionnaire ou une encyclopédie. La tâche n'est pas impossible avec les outils et les méthodes déjà acquis, mais elle est de longue haleine. Heureusement elle est passionnante, car elle permet d'apprendre beaucoup sur \textbf{l'homme} lui-même et son intelligence; c'est bien l'homme, en effet ici, le sujet d'étude fondamental et il est à parier que lorsque la tâche sera accomplie, les programmes d'I.A. du prochain siècle pourront tourner sur les ordinateurs d'aujourd'hui.
\clearpage
