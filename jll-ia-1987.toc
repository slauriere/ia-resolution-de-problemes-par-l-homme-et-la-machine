\select@language {french}
\select@language {french}
\contentsline {chapter}{\numberline {1}Intelligence artificielle}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Statut de l'informatique}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}L'intelligence artificielle}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}D\IeC {\'e}finition et domaines de l'intelligence artificielle}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}D\IeC {\'e}finition}{2}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Domaines}{3}{subsection.1.3.2}
\contentsline {subsubsection}{\numberline {1.3.2.1}La perception et la reconnaissance des formes}{3}{subsubsection.1.3.2.1}
\contentsline {subsubsection}{\numberline {1.3.2.2}Les math\IeC {\'e}matiques et la d\IeC {\'e}monstration automatique de th\IeC {\'e}or\IeC {\`e}mes}{3}{subsubsection.1.3.2.2}
\contentsline {subsubsection}{\numberline {1.3.2.3}Les jeux}{4}{subsubsection.1.3.2.3}
\contentsline {subsubsection}{\numberline {1.3.2.4}La r\IeC {\'e}solution de probl\IeC {\`e}mes}{4}{subsubsection.1.3.2.4}
\contentsline {subsubsection}{\numberline {1.3.2.5}Compr\IeC {\'e}hension du langage naturel (LN)}{4}{subsubsection.1.3.2.5}
\contentsline {section}{\numberline {1.4}Historique}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}R\IeC {\'e}sultats}{6}{section.1.5}
\contentsline {chapter}{\numberline {2}Repr\IeC {\'e}senter un probl\IeC {\`e}me}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Le langage naturel}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Poser un probl\IeC {\`e}me}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Les \IeC {\'e}nonc\IeC {\'e}s ferm\IeC {\'e}s}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}La d\IeC {\'e}marche g\IeC {\'e}n\IeC {\'e}rale en r\IeC {\'e}solution de probl\IeC {\`e}me}{12}{section.2.5}
\contentsline {section}{\numberline {2.6}Un exemple complet}{15}{section.2.6}
\contentsline {section}{\numberline {2.7}Pour r\IeC {\'e}soudre un probl\IeC {\`e}me vous devez successivement}{18}{section.2.7}
\contentsline {section}{\numberline {2.8}Petite histoire des math\IeC {\'e}matiques et de leur enseignement}{19}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Les notations au cours des \IeC {\^a}ges}{19}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}L'enseignement}{24}{subsection.2.8.2}
\contentsline {section}{\numberline {2.9}Les repr\IeC {\'e}sentations}{25}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Les notations math\IeC {\'e}matiques}{25}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Les notations lin\IeC {\'e}aires}{26}{subsection.2.9.2}
\contentsline {subsection}{\numberline {2.9.3}Notations non lin\IeC {\'e}aires}{28}{subsection.2.9.3}
\contentsline {subsection}{\numberline {2.9.4}Repr\IeC {\'e}sentation en machine des expressions}{29}{subsection.2.9.4}
\contentsline {section}{\numberline {2.10}Les mod\IeC {\'e}lisation graphiques en Intelligence Artificielle}{32}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}Repr\IeC {\'e}sentation des connaissances en d\IeC {\'e}monstration automatique:}{32}{subsection.2.10.1}
\contentsline {subsection}{\numberline {2.10.2}Repr\IeC {\'e}sentations graphiques en r\IeC {\'e}solution automatique de probl\IeC {\`e}mes:}{37}{subsection.2.10.2}
\contentsline {subsection}{\numberline {2.10.3}Repr\IeC {\'e}sentations graphiques en compr\IeC {\'e}hension du langage naturel :}{38}{subsection.2.10.3}
\contentsline {section}{\numberline {2.11}Savoir changer de repr\IeC {\'e}sentations}{40}{section.2.11}
\contentsline {section}{\numberline {2.12}Le langage LISP}{42}{section.2.12}
\contentsline {subsection}{\numberline {2.12.1}El\IeC {\'e}ments de base du langage LISP}{42}{subsection.2.12.1}
\contentsline {subsection}{\numberline {2.12.2}Repr\IeC {\'e}sentation et \IeC {\'e}valuation}{44}{subsection.2.12.2}
\contentsline {subsection}{\numberline {2.12.3}Autres fonctions}{47}{subsection.2.12.3}
\contentsline {subsubsection}{\numberline {2.12.3.1}\textbf {Les pr\IeC {\'e}dicats} (valeur T ou NIL)}{47}{subsubsection.2.12.3.1}
\contentsline {subsubsection}{\numberline {2.12.3.2}Les fonctions}{48}{subsubsection.2.12.3.2}
\contentsline {subsubsection}{\numberline {2.12.3.3}Les fonctions num\IeC {\'e}riques :}{49}{subsubsection.2.12.3.3}
\contentsline {subsubsection}{\numberline {2.12.3.4}L'it\IeC {\'e}ration : MAPCAR et APPLY}{50}{subsubsection.2.12.3.4}
\contentsline {subsection}{\numberline {2.12.4}Lambda d\IeC {\'e}finition et propri\IeC {\'e}t\IeC {\'e}s}{50}{subsection.2.12.4}
\contentsline {subsubsection}{\numberline {2.12.4.1}Lambda d\IeC {\'e}finition}{50}{subsubsection.2.12.4.1}
\contentsline {subsubsection}{\numberline {2.12.4.2}Propri\IeC {\'e}t\IeC {\'e}s attach\IeC {\'e}es aux atomes : PUTPROP et GET}{50}{subsubsection.2.12.4.2}
\contentsline {subsection}{\numberline {2.12.5}Constructions nouvelles}{51}{subsection.2.12.5}
\contentsline {subsubsection}{\numberline {2.12.5.1}Fonctions FEXPR et MACRO}{53}{subsubsection.2.12.5.1}
\contentsline {section}{\numberline {2.13}Les graphes}{54}{section.2.13}
\contentsline {chapter}{\numberline {3}Les Syst\IeC {\`e}mes Formels}{57}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{57}{section.3.1}
\contentsline {section}{\numberline {3.2}D\IeC {\'e}finition d'un syst\IeC {\`e}me formel}{58}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Quelques d\IeC {\'e}finitions suppl\IeC {\'e}mentaires}{59}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}R\IeC {\`e}gle du jeu valable dans tout SF: r\IeC {\`e}gle de substitution}{60}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Exemples de S.F.}{60}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Int\IeC {\'e}r\IeC {\^e}t des syst\IeC {\`e}mes formels : D\IeC {\'e}cidabilit\IeC {\'e} et interpr\IeC {\'e}tation}{61}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}D\IeC {\'e}cidabilit\IeC {\'e}}{61}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Interpr\IeC {\'e}tation}{64}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}D\IeC {\'e}monstration et valeurs de v\IeC {\'e}rit\IeC {\'e}:}{64}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Exemples de d\IeC {\'e}monstrations}{66}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}La logique des propositions: (LP)}{69}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Interpr\IeC {\'e}tation de la logique des propositions}{72}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}D\IeC {\'e}cidabilit\IeC {\'e} de la logique des propositions}{73}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Le calcul des pr\IeC {\'e}dicats du premier ordre}{74}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}L'arithm\IeC {\'e}tique formelle}{76}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Deuxi\IeC {\`e}me th\IeC {\'e}or\IeC {\`e}me de G\IeC {\"o}del (1931): Incompl\IeC {\'e}tude de l'arithm\IeC {\'e}tique.}{78}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}M\IeC {\'e}canisme de la preuve de G\IeC {\"o}del}{80}{subsection.3.5.3}
\contentsline {section}{\numberline {3.6}Les th\IeC {\'e}or\IeC {\`e}mes de limitation dans les syst\IeC {\`e}mes formels}{84}{section.3.6}
\contentsline {section}{\numberline {3.7}L'algorithme d'unification}{86}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Le probl\IeC {\`e}me}{86}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Principe de l'algorithme}{88}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}La proc\IeC {\'e}dure de substitution }{90}{subsection.3.7.3}
\contentsline {subsection}{\numberline {3.7.4}Finitude de l'algorithme d'unification}{93}{subsection.3.7.4}
\contentsline {subsection}{\numberline {3.7.5}\begin@guill Variables\end@guill non substituables: ind\IeC {\'e}termin\IeC {\'e}es.}{95}{subsection.3.7.5}
\contentsline {subsection}{\numberline {3.7.6}Mise en oeuvre de l'unification}{97}{subsection.3.7.6}
\contentsline {section}{\numberline {3.8}Utilisation de l'unification}{100}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Probl\IeC {\`e}mes d'analogie.}{100}{subsection.3.8.1}
\contentsline {section}{\numberline {3.9}Utilisation de l'unification}{101}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Probl\IeC {\`e}mes d'analogie.}{102}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Commentaires sur ce type de probl\IeC {\`e}mes de tests g\IeC {\'e}om\IeC {\'e}triques}{105}{subsection.3.9.2}
\contentsline {subsection}{\numberline {3.9.3}Utilisation de l'algorithme d'unification en d\IeC {\'e}monstration automatique}{108}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}La m\IeC {\'e}thode de Sikl\'ossy et Marinov}{108}{subsection.3.9.4}
\contentsline {subsection}{\numberline {3.9.5}R\IeC {\'e}solution d'\IeC {\'e}quations trigonom\IeC {\^e}triques: PRET (M. Grandbastfen (1974)).}{111}{subsection.3.9.5}
\contentsline {subsection}{\numberline {3.9.6}R\IeC {\'e}solution d'exercices d'arithm\IeC {\'e}tique: PARI (D. Bourgoln (1978))}{116}{subsection.3.9.6}
\contentsline {section}{\numberline {3.10}Le programme de J. PITRAT en logique des propositions}{120}{section.3.10}
\contentsline {subsection}{\numberline {3.10.1}Hi\IeC {\'e}rarchie des m\IeC {\^e}tath\IeC {\^e}ories}{120}{subsection.3.10.1}
\contentsline {subsection}{\numberline {3.10.2}Les m\IeC {\'e}tath\IeC {\^e}ories du programme :}{121}{subsection.3.10.2}
\contentsline {subsection}{\numberline {3.10.3}Choix des essais :}{123}{subsection.3.10.3}
\contentsline {section}{\numberline {3.11}Le Principe de R\IeC {\'e}solution et le Langage PROLOG}{124}{section.3.11}
\contentsline {subsection}{\numberline {3.11.1}Le Principe de R\IeC {\'e}solution}{124}{subsection.3.11.1}
\contentsline {subsection}{\numberline {3.11.2}Th\IeC {\'e}or\IeC {\`e}me d'Herbrand}{126}{subsection.3.11.2}
\contentsline {subsection}{\numberline {3.11.3}Organisation pratique des d\IeC {\'e}monstrations par le principe d'Herbrand}{128}{subsection.3.11.3}
\contentsline {subsubsection}{\numberline {3.11.3.1}Mise sous forme normale conjonctive.}{128}{subsubsection.3.11.3.1}
\contentsline {subsection}{\numberline {3.11.4}Exemples de d\IeC {\'e}monstrations suivant le principe d'Herbrand :}{130}{subsection.3.11.4}
\contentsline {subsection}{\numberline {3.11.5}Analyse de ces d\IeC {\'e}monstrations.}{133}{subsection.3.11.5}
\contentsline {subsection}{\numberline {3.11.6}Strat\IeC {\'e}gies}{133}{subsection.3.11.6}
\contentsline {subsection}{\numberline {3.11.7}Int\IeC {\'e}r\IeC {\^e}ts et limites de la R\IeC {\'e}solution.}{134}{subsection.3.11.7}
\contentsline {subsection}{\numberline {3.11.8}Le langage PROLOG et les applications de la m\IeC {\'e}thode de R\IeC {\'e}solution}{135}{subsection.3.11.8}
\contentsline {chapter}{\numberline {4}Les m\IeC {\'e}thodes classiques en r\IeC {\'e}solution de probl\IeC {\`e}mes}{139}{chapter.4}
\contentsline {section}{\numberline {4.1}Exemples de bons algorithmes}{139}{section.4.1}
\contentsline {section}{\numberline {4.2}Liste des probl\IeC {\`e}mes bien r\IeC {\'e}solus (algorithmes polyn\IeC {\^o}miaux)}{157}{section.4.2}
\contentsline {section}{\numberline {4.3} Le classement des probl\IeC {\`e}mes selon leur complexit\IeC {\'e}}{160}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Trois classes de probl\IeC {\`e}mes : }{161}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}La classe des probl\IeC {\`e}mes NP : non d\IeC {\'e}terministes polyn\IeC {\^o}miaux}{163}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Deux familles de machines}{163}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Liste de probl\IeC {\`e}mes dans $NP$}{166}{section.4.5}
\contentsline {section}{\numberline {4.6}Etude des probl\IeC {\`e}mes de $NP$ par classes d'\IeC {\'e}quivalence}{168}{section.4.6}
\contentsline {section}{\numberline {4.7}Le th\IeC {\'e}or\IeC {\`e}me fondamental (Cook (1971))}{168}{section.4.7}
\contentsline {section}{\numberline {4.8}La classe des $NP$-complets}{171}{section.4.8}
\contentsline {section}{\numberline {4.9}Quelques d\IeC {\'e}monstrations d'\IeC {\'e}quivalences entre probl\IeC {\`e}mes}{173}{section.4.9}
\contentsline {chapter}{\numberline {5}Les m\IeC {\'e}thodes par propagation et \IeC {\'e}num\IeC {\'e}ration}{179}{chapter.5}
\contentsline {section}{\numberline {5.1}Les m\IeC {\'e}thodes de r\IeC {\'e}solution par \IeC {\'e}num\IeC {\'e}ration}{179}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}La classe des probl\IeC {\`e}mes combinatoires}{179}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Les m\IeC {\'e}thodes de gradient}{180}{section.5.2}
\contentsline {section}{\numberline {5.3}La programmation lin\IeC {\'e}aire}{182}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Formalisation de l'algorithme du simplexe}{185}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}M\IeC {\'e}thodes de gradient en th\IeC {\'e}orie des graphes}{185}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}D\IeC {\'e}monstration de l'algorithme de Kruskal}{186}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Recherche heuristique}{187}{section.5.5}
\contentsline {section}{\numberline {5.6}L'algorithme $A^*$}{189}{section.5.6}
\contentsline {section}{\numberline {5.7}L'\IeC {\'e}num\IeC {\'e}ration implicite par propagation de contraintes}{194}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Huit dames, premi\IeC {\`e}re version.}{197}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}Implications li\IeC {\'e}es \IeC {\`a} un choix}{198}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Huit dames, version 2.}{199}{subsection.5.7.3}
\contentsline {section}{\numberline {5.8}Choix de l'ordre des choix}{201}{section.5.8}
\contentsline {section}{\numberline {5.9}Vers la programmation dynamique}{205}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Th\IeC {\'e}orie de la programmation dynamique }{206}{subsection.5.9.1}
\contentsline {subsection}{\numberline {5.9.2}Int\IeC {\'e}r\IeC {\^e}t de la programmation dynamique}{207}{subsection.5.9.2}
\contentsline {section}{\numberline {5.10}Coloration optimale des sommets d'un graphe}{209}{section.5.10}
\contentsline {subsection}{\numberline {5.10.1}Organisation d'un colloque}{210}{subsection.5.10.1}
\contentsline {section}{\numberline {5.11}Algorithme de coloration optimale d'un graphe}{211}{section.5.11}
\contentsline {subsection}{\numberline {5.11.1}Premi\IeC {\`e}re \IeC {\'e}tape: recherche d'une solution}{212}{subsection.5.11.1}
\contentsline {subsection}{\numberline {5.11.2}Deuxi\IeC {\`e}me \IeC {\'e}tape : recherche de l'optimalit\IeC {\'e}}{215}{subsection.5.11.2}
\contentsline {section}{\numberline {5.12}Le probl\IeC {\`e}me du voyageur de commerce}{219}{section.5.12}
\contentsline {subsection}{\numberline {5.12.1}Premi\IeC {\`e}re \IeC {\'e}tape: recherche d'une solution}{220}{subsection.5.12.1}
\contentsline {subsection}{\numberline {5.12.2}Seconde \IeC {\'e}tape: recherche de l'optimum}{222}{subsection.5.12.2}
\contentsline {section}{\numberline {5.13}Un programme g\IeC {\'e}n\IeC {\'e}ral de r\IeC {\'e}solution de probl\IeC {\`e}mes}{227}{section.5.13}
\contentsline {subsection}{\numberline {5.13.1}La d\IeC {\'e}marche g\IeC {\'e}n\IeC {\'e}rale de GPS: les sous-probl\IeC {\`e}mes et les plans}{227}{subsection.5.13.1}
\contentsline {subsection}{\numberline {5.13.2}Un premier probl\IeC {\`e}me}{228}{subsection.5.13.2}
\contentsline {subsection}{\numberline {5.13.3}Un probl\IeC {\`e}me de logique formelle}{232}{subsection.5.13.3}
\contentsline {subsection}{\numberline {5.13.4}Autres t\IeC {\^a}ches donn\IeC {\'e}es \IeC {\`a} GPS}{238}{subsection.5.13.4}
\contentsline {subsection}{\numberline {5.13.5}Conclusion}{243}{subsection.5.13.5}
\contentsline {chapter}{\numberline {6}Les programmes de jeux ; les \IeC {\'e}tudes des psychologues}{245}{chapter.6}
\contentsline {section}{\numberline {6.1}L'arborescence des coups l\IeC {\'e}gaux}{246}{section.6.1}
\contentsline {section}{\numberline {6.2}L'\IeC {\'e}valuation des positions}{253}{section.6.2}
\contentsline {section}{\numberline {6.3}Le \textit {minmax} et le choix du coup \IeC {\`a} jouer}{254}{section.6.3}
\contentsline {section}{\numberline {6.4}La proc\IeC {\'e}dure Alpha-B\IeC {\^e}ta}{259}{section.6.4}
\contentsline {section}{\numberline {6.5}Les d\IeC {\'e}fauts fondamentaux li\IeC {\'e}s au d\IeC {\'e}veloppement syst\IeC {\'e}matique de l'arbre des coups}{262}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Premier d\IeC {\'e}faut: le manque total de strat\IeC {\'e}gie}{262}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Deuxi\IeC {\`e}me d\IeC {\'e}faut: l'effet d'horizon}{263}{subsection.6.5.2}
\contentsline {section}{\numberline {6.6}Les \IeC {\'e}tudes des psychologues sur la r\IeC {\'e}solution de probl\IeC {\`e}mes par les hommes}{264}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}La m\IeC {\'e}moire}{265}{subsection.6.6.1}
\contentsline {subsection}{\numberline {6.6.2}La perception}{266}{subsection.6.6.2}
\contentsline {section}{\numberline {6.7}Les \IeC {\'e}tudes des psychologues sur les joueurs d'\IeC {\'e}checs}{267}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}La perception}{267}{subsection.6.7.1}
\contentsline {subsection}{\numberline {6.7.2}Le raisonnement}{269}{subsection.6.7.2}
\contentsline {section}{\numberline {6.8}ROBIN : Un programme d'\IeC {\'e}chec intelligent}{273}{section.6.8}
\contentsline {subsection}{\numberline {6.8.1}Un exemple d'analyse par ROBIN:}{274}{subsection.6.8.1}
\contentsline {subsection}{\numberline {6.8.2}Diff\IeC {\'e}rents types de plans aux \IeC {\'e}checs}{276}{subsection.6.8.2}
\contentsline {subsection}{\numberline {6.8.3}Langage d'expression et d'ex\IeC {\'e}cution des plans:}{277}{subsection.6.8.3}
\contentsline {subsection}{\numberline {6.8.4}L'optimisation de la d\IeC {\'e}fense: la notion de coup dangereux}{279}{subsection.6.8.4}
\contentsline {subsection}{\numberline {6.8.5}R\IeC {\'e}sultats}{282}{subsection.6.8.5}
\contentsline {chapter}{\numberline {7}Les syst\IeC {\`e}mes experts}{287}{chapter.7}
\contentsline {section}{\numberline {7.1}Exemples de dialogue avec un syst\IeC {\`e}me \begin@guill expert \end@guill }{289}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Un diagnostic m\IeC {\'e}dical}{289}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Explication, par le syst\IeC {\`e}me, de son propre raisonnement}{289}{subsection.7.1.2}
\contentsline {section}{\numberline {7.2}Le fonctionnement du syst\IeC {\`e}me MYCIN-TEIRESIAS}{290}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Le but}{290}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}L'int\IeC {\'e}r\IeC {\^e}t}{290}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Description}{291}{subsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.3.1}Les r\IeC {\`e}gles}{292}{subsubsection.7.2.3.1}
\contentsline {subsubsection}{\numberline {7.2.3.2}Le raisonnement approch\IeC {\'e}}{292}{subsubsection.7.2.3.2}
\contentsline {subsubsection}{\numberline {7.2.3.3}Le m\IeC {\'e}canisme d'inf\IeC {\'e}rence}{293}{subsubsection.7.2.3.3}
\contentsline {subsubsection}{\numberline {7.2.3.4}Les m\IeC {\'e}tar\IeC {\`e}gles}{294}{subsubsection.7.2.3.4}
\contentsline {subsection}{\numberline {7.2.4}Explications fournies par MYCIN et dialogue en langue naturelle }{295}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}L'acquisition de nouvelles connaissances : TEIRESIAS}{296}{subsection.7.2.5}
\contentsline {subsubsection}{\numberline {7.2.5.1}Exemple comment\IeC {\'e} de TEIRESIAS au travail}{296}{subsubsection.7.2.5.1}
\contentsline {subsubsection}{\numberline {7.2.5.2}Fonctionnement}{299}{subsubsection.7.2.5.2}
\contentsline {subsection}{\numberline {7.2.6}R\IeC {\'e}sultats et Analyse critique}{301}{subsection.7.2.6}
\contentsline {section}{\numberline {7.3}Les syst\IeC {\`e}mes de production}{301}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Description d'un syst\IeC {\`e}me de production}{303}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Hypoth\IeC {\`e}ses et domaines d'applicabilit\IeC {\'e} des syst\IeC {\`e}mes de production}{305}{subsection.7.3.2}
\contentsline {subsubsection}{\numberline {7.3.2.1}R\IeC {\^o}le du sp\IeC {\'e}cialiste}{305}{subsubsection.7.3.2.1}
\contentsline {subsubsection}{\numberline {7.3.2.2}Structure de l'univers \IeC {\'e}tudi\IeC {\'e}}{306}{subsubsection.7.3.2.2}
\contentsline {subsubsection}{\numberline {7.3.2.3}Sch\IeC {\'e}mas d'inf\IeC {\'e}rence}{306}{subsubsection.7.3.2.3}
\contentsline {subsubsection}{\numberline {7.3.2.4}Cons\IeC {\'e}quences a priori de ces hypoth\IeC {\`e}ses}{306}{subsubsection.7.3.2.4}
\contentsline {subsection}{\numberline {7.3.3}Variantes utilis\IeC {\'e}es dans la conception des syst\IeC {\`e}mes de production}{306}{subsection.7.3.3}
\contentsline {subsubsection}{\numberline {7.3.3.1}Le contenu de la base d'expertise}{307}{subsubsection.7.3.3.1}
\contentsline {subsubsection}{\numberline {7.3.3.2}La structure de contr\IeC {\^o}le}{307}{subsubsection.7.3.3.2}
\contentsline {subsubsection}{\numberline {7.3.3.3}Co\IeC {\^u}t d'ex\IeC {\'e}cution d'un syst\IeC {\`e}me de production et efficacit\IeC {\'e} }{308}{subsubsection.7.3.3.3}
\contentsline {subsection}{\numberline {7.3.4}Diff\IeC {\'e}rents syst\IeC {\`e}mes experts utilisant des r\IeC {\`e}gles de production}{309}{subsection.7.3.4}
\contentsline {subsubsection}{\numberline {7.3.4.1}DENDRAL}{309}{subsubsection.7.3.4.1}
\contentsline {subsubsection}{\numberline {7.3.4.2}META-DENDRAL}{310}{subsubsection.7.3.4.2}
\contentsline {subsubsection}{\numberline {7.3.4.3}Le programme de Waterman (1970)}{310}{subsubsection.7.3.4.3}
\contentsline {subsubsection}{\numberline {7.3.4.4}La famille MYCIN-TEIRESIAS}{311}{subsubsection.7.3.4.4}
\contentsline {subsubsection}{\numberline {7.3.4.5}SU/X et quelques autres}{312}{subsubsection.7.3.4.5}
\contentsline {subsubsection}{\numberline {7.3.4.6}Syst\IeC {\`e}mes G\IeC {\'e}n\IeC {\'e}raux}{314}{subsubsection.7.3.4.6}
\contentsline {subsubsection}{\numberline {7.3.4.7}Syst\IeC {\`e}mes avec apprentissage}{316}{subsubsection.7.3.4.7}
\contentsline {section}{\numberline {7.4}Les syst\IeC {\`e}mes experts fond\IeC {\'e}s sur la logique du premier ordre}{319}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Le Langage PROLOG}{319}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}PEACE : un exemple de programme expert en PROLOG }{321}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}Second exemple :}{323}{subsection.7.4.3}
\contentsline {subsubsection}{\numberline {7.4.3.1}Un exemple}{323}{subsubsection.7.4.3.1}
\contentsline {subsubsection}{\numberline {7.4.3.2}Description du syst\IeC {\`e}me}{324}{subsubsection.7.4.3.2}
\contentsline {subsubsection}{\numberline {7.4.3.3}R\IeC {\'e}sultats}{327}{subsubsection.7.4.3.3}
\contentsline {subsection}{\numberline {7.4.4}Int\IeC {\'e}r\IeC {\^e}t de PROLOG et du Calcul des Pr\IeC {\'e}dicats}{328}{subsection.7.4.4}
\contentsline {subsubsection}{\numberline {7.4.4.1}Un langage de contr\IeC {\^o}le pour PROLOG}{329}{subsubsection.7.4.4.1}
\contentsline {subsubsection}{\numberline {7.4.4.2}Comparaison entre le calcul des pr\IeC {\'e}dicats en PROLOG et les syst\IeC {\`e}mes de production}{329}{subsubsection.7.4.4.2}
\contentsline {subsection}{\numberline {7.4.5}SNARK : un autre exemple de langage du premier ordre}{330}{subsection.7.4.5}
\contentsline {section}{\numberline {7.5}La controverse d\IeC {\'e}claratif/proc\IeC {\'e}dural}{336}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Critique de l'informatique {\begin@guill }proc\IeC {\'e}durale{\end@guill } ou {\begin@guill }les langages de programmation ne sont pas des langages{\end@guill } }{337}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}Les structures de contr\IeC {\^o}le dans ces syst\IeC {\`e}mes experts}{338}{subsection.7.5.2}
\contentsline {subsection}{\numberline {7.5.3}R\IeC {\`e}gles de production et proc\IeC {\'e}dures : exemples}{339}{subsection.7.5.3}
\contentsline {subsection}{\numberline {7.5.4}Efficacit\IeC {\'e} des syst\IeC {\`e}mes experts}{341}{subsection.7.5.4}
\contentsline {subsection}{\numberline {7.5.5}Conclusion sur la controverse proc\IeC {\'e}dural/d\IeC {\'e}claratif}{345}{subsection.7.5.5}
\contentsline {section}{\numberline {7.6}Les diff\IeC {\'e}rents types de connaissances et leurs repr\IeC {\'e}sentations}{345}{section.7.6}
\contentsline {subsection}{\numberline {7.6.1}Les diff\IeC {\'e}rents types de connaissances }{346}{subsection.7.6.1}
\contentsline {subsection}{\numberline {7.6.2}Les diff\IeC {\'e}rentes repr\IeC {\'e}sentations de la connaissance dans les syst\IeC {\`e}mes actuels}{347}{subsection.7.6.2}
\contentsline {subsection}{\numberline {7.6.3}Acc\IeC {\`e}s et modes d'utilisation des connaissances}{348}{subsection.7.6.3}
\contentsline {subsection}{\numberline {7.6.4}Avantages et inconv\IeC {\'e}nients des syst\IeC {\`e}mes de production}{349}{subsection.7.6.4}
\contentsline {section}{\numberline {7.7}La m\IeC {\'e}ta-connaissance}{351}{section.7.7}
\contentsline {subsection}{\numberline {7.7.1}M\IeC {\'e}ta-connaissance des objets de l'univers}{351}{subsection.7.7.1}
\contentsline {subsection}{\numberline {7.7.2}M\IeC {\'e}ta-connaissance des strat\IeC {\'e}gies}{352}{subsection.7.7.2}
\contentsline {section}{\numberline {7.8}Conclusion}{353}{section.7.8}
\contentsline {section}{\numberline {7.9}Votre moteur de syst\IeC {\`e}me expert en kit}{356}{section.7.9}
\contentsline {chapter}{\numberline {8}ALICE}{363}{chapter.8}
\contentsline {section}{\numberline {8.1}Description g\IeC {\'e}n\IeC {\'e}rale du syst\IeC {\`e}me}{364}{section.8.1}
\contentsline {section}{\numberline {8.2}Le langage ALICE}{365}{section.8.2}
\contentsline {section}{\numberline {8.3}Le module de r\IeC {\'e}solution de probl\IeC {\`e}me}{370}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}La propagation des contraintes}{371}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Faire des choix}{373}{subsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.2.1}Diff\IeC {\'e}rents types de choix et crit\IeC {\`e}res de choix}{373}{subsubsection.8.3.2.1}
\contentsline {subsubsection}{\numberline {8.3.2.2}Plan g\IeC {\'e}n\IeC {\'e}ral de la r\IeC {\'e}solution}{374}{subsubsection.8.3.2.2}
\contentsline {subsection}{\numberline {8.3.3}R\IeC {\'e}solution des exercices}{375}{subsection.8.3.3}
\contentsline {section}{\numberline {8.4}R\IeC {\'e}sultats}{378}{section.8.4}
\contentsline {section}{\numberline {8.5}ALICE : expos\IeC {\'e} d\IeC {\'e}taill\IeC {\'e}}{378}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}La repr\IeC {\'e}sentation des probl\IeC {\`e}mes}{378}{subsection.8.5.1}
\contentsline {subsubsection}{\numberline {8.5.1.1}Repr\IeC {\'e}sentation graphique}{379}{subsubsection.8.5.1.1}
\contentsline {subsubsection}{\numberline {8.5.1.2}Repr\IeC {\'e}sentation formelle}{379}{subsubsection.8.5.1.2}
\contentsline {subsubsection}{\numberline {8.5.1.3}Passage de l'information entre deux repr\IeC {\'e}sentations}{381}{subsubsection.8.5.1.3}
\contentsline {subsection}{\numberline {8.5.2}Le traitement des contraintes alg\IeC {\'e}briques}{382}{subsection.8.5.2}
\contentsline {subsubsection}{\numberline {8.5.2.1}La proc\IeC {\'e}dure g\IeC {\'e}n\IeC {\'e}rale d'analyse d'une contrainte}{382}{subsubsection.8.5.2.1}
\contentsline {subsubsection}{\numberline {8.5.2.2}Cas particuliers}{383}{subsubsection.8.5.2.2}
\contentsline {subsubsection}{\numberline {8.5.2.3}Ordre de traitement des contraintes}{385}{subsubsection.8.5.2.3}
\contentsline {subsection}{\numberline {8.5.3}Les proc\IeC {\'e}dures de choix}{386}{subsection.8.5.3}
\contentsline {subsubsection}{\numberline {8.5.3.1}Choix du type de choix}{387}{subsubsection.8.5.3.1}
\contentsline {subsubsection}{\numberline {8.5.3.2}Ordonnancement des crit\IeC {\`e}res et strat\IeC {\'e}gie d'ALICE}{389}{subsubsection.8.5.3.2}
\contentsline {subsubsection}{\numberline {8.5.3.3}Preuves d'optimalit\IeC {\'e}}{390}{subsubsection.8.5.3.3}
\contentsline {section}{\numberline {8.6}Exemples d\IeC {\'e}taill\IeC {\'e}s de r\IeC {\'e}solutions}{392}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Un casse-t\IeC {\^e}te logico-arithm\IeC {\'e}tique}{392}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Un probl\IeC {\`e}me de fabrication de papier}{393}{subsection.8.6.2}
\contentsline {section}{\numberline {8.7}Table g\IeC {\'e}n\IeC {\'e}rale des probl\IeC {\`e}mes trait\IeC {\'e}s par ALICE :}{400}{section.8.7}
\contentsline {section}{\numberline {8.8}G\IeC {\'e}n\IeC {\'e}ralit\IeC {\'e} et efficacit\IeC {\'e}}{402}{section.8.8}
\contentsline {subsection}{\numberline {8.8.1}Exemple 1}{403}{subsection.8.8.1}
\contentsline {subsection}{\numberline {8.8.2}Exemple 2}{407}{subsection.8.8.2}
\contentsline {subsection}{\numberline {8.8.3}Ex\IeC {\'e}cution en ALICE}{410}{subsection.8.8.3}
\contentsline {section}{\numberline {8.9}Saisie des \IeC {\'e}nonc\IeC {\'e}s en fran\IeC {\c c}ais}{412}{section.8.9}
\contentsline {subsection}{\numberline {8.9.1}Principe de la traduction}{412}{subsection.8.9.1}
\contentsline {subsection}{\numberline {8.9.2}Exemple de traduction}{414}{subsection.8.9.2}
\contentsline {chapter}{\numberline {9}L'apprentissage}{417}{chapter.9}
\contentsline {section}{\numberline {9.1}Diff\IeC {\'e}rents types d'apprentissages}{417}{section.9.1}
\contentsline {section}{\numberline {9.2}Apprentissage de param\IeC {\`e}tres au jeu de checkers}{419}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Apprentissage par coeur}{419}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Apprentissage livresque}{420}{subsection.9.2.2}
\contentsline {subsection}{\numberline {9.2.3}Apprentissage sans professeur}{421}{subsection.9.2.3}
\contentsline {subsection}{\numberline {9.2.4}Apprentissage g\IeC {\'e}n\IeC {\'e}ralis\IeC {\'e}}{421}{subsection.9.2.4}
\contentsline {section}{\numberline {9.3}Apprentissage d'actions utiles par un robot: STRIPS}{424}{section.9.3}
\contentsline {section}{\numberline {9.4}Apprentissage de r\IeC {\`e}gles}{425}{section.9.4}
\contentsline {section}{\numberline {9.5}Apprentissage de plans}{429}{section.9.5}
\contentsline {section}{\numberline {9.6}Apprentissage de caract\IeC {\'e}ristiques}{435}{section.9.6}
\contentsline {section}{\numberline {9.7}Apprentissage de concepts}{437}{section.9.7}
